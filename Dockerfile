FROM openjdk:12-alpine
#COPY ./build/libs/cars-api.jar
COPY --from=MAVEN_BUILD /build/libs/cars-api.jar /app/cars-api.jar
ENTRYPOINT ["java", "-jar", "cars-api.jar"]